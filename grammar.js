const C = require('tree-sitter-c/grammar')

module.exports = grammar(C, {
    name: 'opencl',

    rules: {
        _top_level_item: (_, original) => original,

        function_definition: ($, original) => seq(
            choice(
                "kernel", "__kernel",
            ),
            original,
        ),

        _image_type_qualifier: $ => choice(
            'read_only', '__read_only',
            'write_only', '__write_only',
            'read_write', '__read_write',
        ),

        type_qualifier: ($, original) => choice(
            original,
            'complex',
            'imaginary',
            'global', '__global',
            'local', '__local',
            'constant', '__constant',
            'private', '__private',
            'pipe',
            $._image_type_qualifier,
        ),

        // https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_C.html#built-in-scalar-data-types
        _scalar_primitive_type: $ => choice(
            'bool',
            'char',
            'uchar',
            // 'short',
            'ushort',
            'int',
            'uint',
            // 'long',
            'ulong',
            'float',
            'double',
            'half',
            'size_t',
            'ptrdiff_t',
            'intptr_t',
            'uintptr_t',
            'void',
        ),

        // https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_C.html#built-in-vector-data-types
        _vector_primitive_type: $ => choice(
            ...[2, 3, 4, 8, 16].map(n => `char${n}`),
            ...[2, 3, 4, 8, 16].map(n => `uchar${n}`),
            ...[2, 3, 4, 8, 16].map(n => `short${n}`),
            ...[2, 3, 4, 8, 16].map(n => `ushort${n}`),
            ...[2, 3, 4, 8, 16].map(n => `int${n}`),
            ...[2, 3, 4, 8, 16].map(n => `uint${n}`),
            ...[2, 3, 4, 8, 16].map(n => `long${n}`),
            ...[2, 3, 4, 8, 16].map(n => `ulong${n}`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}`),
        ),

        // https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_C.html#other-built-in-data-types
        _image_primitive_type: $ => choice(
            'image2d_t',
            'image3d_t',
            'image1d_t',
        ),
        _builtin_primitive_type: $ => choice(
            'image2d_array_t',
            'image1d_buffer_t',
            'image1d_array_t',
            'image2d_depth_t',
            'image2d_array_depth_t',
            'sampler_t',
            'queue_t',
            'ndrange_t',
            'clk_event_t',
            'reserve_id_t',
            'event_t',
            'cl_mem_fence_flags',
        ),

        // https://www.khronos.org/registry/OpenCL/specs/3.0-unified/html/OpenCL_C.html#reserved-data-types
        _reserved_primitive_type: $ => choice(
            'quad',
            ...[2, 3, 4, 8, 16].map(n => `bool${n}`),
            ...[2, 3, 4, 8, 16].map(n => `half${n}`),
            ...[2, 3, 4, 8, 16].map(n => `quad${n}`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}x2`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}x3`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}x4`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}x8`),
            ...[2, 3, 4, 8, 16].map(n => `float${n}x16`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}x2`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}x3`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}x4`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}x8`),
            ...[2, 3, 4, 8, 16].map(n => `double${n}x16`),
        ),

        primitive_type: $ => choice(
            $._scalar_primitive_type,
            $._vector_primitive_type,
            $._image_primitive_type,
            $._builtin_primitive_type,
            $._reserved_primitive_type,
        ),
    },
});
